<!doctype html>
<html lang="en">

<?php include('../layouts2/include.php'); ?>

<body>
<div class="phone-wd">
  <!-- <div class="main_box"> -->
    <?php include('../layouts2/logo.php'); ?>
    <?php include('../layouts2/menu.php'); ?>
    <?php include('../layouts2/banner.php'); ?>

      <div class="box_title">
          <div>
              <p class="fl" style="line-height: 45px;"><a href='../../'>Home</a> <small style="color: #04816f;">></small> <a href='index.php'>Urethritis</a> <small style="color: #04816f;">></small> <a href='#in-here'>Perawatan Urethritis</a></p>
          </div>
      </div>

      <div class="phone mt10">
        <p style="margin-top: 30px;"><span><img src="../assets/img/phone-icon.jpg" width="6%"><a href="tel:081362621616">Klik untuk menghubungi: 0813-6262-1616</a></span></p>
      </div>

      <div class="text_title mt20">
        <h1 style="padding-top: 10px;  margin: 7px;">Cara yang baik untuk mengobati balanitis</h1>
        <p style="padding-bottom: 10px; margin: 7px;">Sumber： Klinik Utama Sentosa <!--Tanggal：2018-03-15--></p>
      </div>

      <div class="text_main ov mt20 text-justify">
          <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->

        <!-- <br><br> -->

        <!-- Start Effect gif -->
        <!-- <div class="effect-gif">
          <a href="##chat" title="konsultasi">
            <strong class="blue-gif">
            Masalah serupa? Konsultasi langsung dengan dokter
            </strong>
            
            <div>
              <img alt="" src="../assets/images/red-right.gif" style="width: 26px; height: 15px;">
              <strong class="blue-second-gif">Konsultasi online gratis</strong> | <strong class="red-gif">Konsultasi telepon</strong>
              <img alt="" src="../assets/images/red-right.gif" style="width: 26px; height: 15px;">
            </div>
          </a>
        </div> -->
        <!-- End Effect gif -->

        <!-- <br>

        <p>
          balanitis adalah yang paling umum laki-laki penyakit disfungsi seksual, di mana laki-laki berhubungan seks dalam situasi, penis tidak bisa ereksi atau ereksi tetapi tidak sulit, tidak bisa dispareunia aktivitas seksual terjadi, penis tidak bisa ereksi sepenuhnya dikenal sebagai balanitis lengkap Namun, seseorang dengan kekerasan yang cukup untuk penis harus tegak tetapi tidak melakukan hubungan seksual disebut impoten tidak lengkap.
        </p>

        <br>

        <p>
          <span class="red-text">
            Pakar pria menunjukkan bahwa: Saat ini ada tiga metode utama untuk pengobatan balanitis: satu pengobatan adalah penggunaan obat, dua perawatan adalah terapi fisik seperti alat tekanan negatif vakum, dan tiga jenis perawatan terutama merujuk pada perawatan bedah.
          </span>
          <span class="blue-strong-text">
            Metode perawatan spesifik tergantung pada kondisi sebenarnya, dokter daring dapat membantu Anda menganalisa metode perawatan mana yang lebih cocok untuk Anda
          </span>
        </p>

        <br> -->

        <div class="article">

          <h3>Perawatan Penyakit Balanitis</h3>

          <br>

          <h6><b>Bagaimana Balanitis Diobati?</b></h6>

          <p>
            Balanitis adalah suatu kondisi yang menyebabkan kepala (glans) alat kelamin pria membengkak yang membuat tidak nyaman. Ada sejumlah kemungkinan penyebabnya, oleh karena itu segera konsultasikan segera dengan dokter untuk diketahui penyebab utamanya dan segera ditindak lanjuti agar permasalahannya segera bisa diobati.
          </p>
          <p>
            Pada pria yang telah di khitan, radang seperti itu sangat kurang umum dan terbatas pada kepala (glans) alat kelamin pria (balanitis). Setiap yang menderita diabetes mellitus (kencing manis) sangat rentan terhadap kondisi ini dan yang menularkan radang harus di evaluasi untuk penyakit diabetes (kencing manis).
          </p>
          <p>
            Kemungkinan yang nyara bahwa penyakit balanitis dan balanoposthitis dapat menjadi pertanda penyakit lain yang lebih serius dan oleh karena itu tidak dapat dipungkiri sebagai salah satu peradangan yang tidak sederhana terutama jika kondisinya menjadi kronis atau kondisi yang secara terus-menerus terjadi.
          </p>
          <p>
            Perawatan balanitis tergantung pada penyebab yang spesifiknya yang dapat bervariasi dari satu kasus ke kasus lainnya. Jika reaksi alergi menimbulkan gejala, maka pencegahan menggunakan produk yang berbahan kimia adalah salah satunya. Yang mana untuk meredakan iritasi pada kulit. Selain itu sangat penting untuk membersihakn alat kelamin pria secara menyeluruh setiap hari untuk mengurangi gejala. Jika kondisinya terus terjadi atau jika peradangan selalu mengganggu saat buang air kecil (BAK atau kencing) maka cara satu-satunya adalah dengan melakukan metode khitan.
          </p>
          
          <!-- Start Effect gif -->
          <br>
          <div class="effect-gif">
            <a href="##chat" title="konsultasi">
              <div>
                <img alt="" src="../assets/images/red-right.gif" style="width: 26px; height: 15px;">
                <strong class="blue-second-gif">jika belum jelas mengenai perawatan untuk penyakit balanitis </strong> | <strong class="red-gif">[klik disini]</strong>
                <img alt="" src="../assets/images/red-right.gif" style="width: 26px; height: 15px;">
              </div>
            </a>
          </div>
          <br>
          <!-- End Effect gif -->

          <h6><b>Perawatan Untuk Penyakit Balanitis</b></h6>

          <p>
            Setelah didiagnosis karena balanitis, maka pemilihan perawatan untuk menghilangkan rasa gatal, menghilangkan jamur dan inflamasi (peradangan atau upaya tubuh untuk perlindungan diri yang tujuannya untuk menghilangkan rangsangan yang berbahaya, termasuk sel-sel yang rusak, iritasi atau parasit dan proses perawatannya).
          </p>
          <p>
            Untuk meredakan balanitis konsultasikan langsung dengan dokter agar diketahui cara cepat untuk membunuh bakteri pada akar infeksinya. Kulit yang meradang sangat rapuh dan mudah berisiko terkena infeksi sekunder. Untuk infeksi tertentu pengobatan anti jamur digunakan untuk mengobati kandida.
          </p>

          <br>
          <img class="responsive-img materialboxed" src="images/Perawatan-Penyakit-Urethritis.JPG">
          <br>

          <p>
            Jika tidak ada penyebab yang dapat ditemukan, maka harus dilakukan pencucian yang dapat membantu gejala dan meredakan peradangan. Metode sunat biasanya direkomendasikan pada pasien yang mempunyai kulup yang sangat ketat sekalinya peradangan telah teratasi. Kondisi yang terkadang sulit untuk dikenali, jika sakit pada kelenjar atau kulup yang berlanjut maka biopsi (pengambilan jaringan tubuh untuk pemeriksaan laboratorium) diindikasikan untuk kondisi yang ganas. Kondisi yang diperlukan untuk pertumbuhan organisme yang terkena paparan sederhana terhadap pembersihan udara dan yang paling sering efektif. 
          </p>

          <!-- Start Effect gif -->
          <br>
          <div class="effect-gif">
            <a href="##chat" title="konsultasi">
              <div>
                <img alt="" src="../assets/images/red-right.gif" style="width: 26px; height: 15px;">
                <strong class="blue-second-gif">butuh bantuan chat online </strong> | <strong class="red-gif">[klik disini untuk konsultasi dengan dokter]</strong>
                <img alt="" src="../assets/images/red-right.gif" style="width: 26px; height: 15px;">
              </div>
            </a>
          </div>
          <br>
          <!-- End Effect gif -->

          <h6><b>Segera Konsultasikan Pada Klinik Utama Sentosa</b></h6>

          <p>
            Klinik Utama Sentosa adalah klinik spesialis penyakit kelamin yang memiliki peralatan atau alat-alat medis yang sudah modern dan canggih dan bertaraf nasional. Dan ditangani oleh para dokter yang sudah ahli didalam bidangnya, selain itu biaya pengobatan yang sangat terjangkau. Lokasi klinik yang sangat strategis yang berada di Jakarta Indonesia. Sangat mengutamakan kepuasan dan kesembuhan setiap pasiennya dengan memprioritaskan pasiennya nomor 1 dalam setiap pelayanannya.
          </p>

          <p>
            Untuk mengetahui informasi lebih lanjut mengenai penyakit menular seksual (PMS) lainnya segera hubungi hotline kami di nomor <strong class="red-text bolded">0813 6262 1616.</strong>
          </p>

          <p>
            <strong class="red-text bolded">Klinik Utama Sentosa</strong> adalah salah satu klinik spesialis penyakit kelamin yang berada di Jakarta, Indonesia. Menangani setiap permasalahan penyakit kelamin seperti penyakit menular seksual (PMS), infeksi menular seksual (IMS), Andrologi dan Ginekologi. Didukung oleh dokter ahli dan peralatan medis yang modern guna membantu proses pengobatan penyakit kelamin .
          </p>

            <blockquote>
              Disclaimer: Hasil dapat berbeda dari masing-masing individu.
            </blockquote>

          <!-- <p class="untext-indent">
            <strong class="red-text"><b>Tips:</b> </strong> Saya harap pengenalan dokter dari Rumah Sakit Huai'an Pok Oi dapat membantu pria, Jika Anda ingin tahu lebih banyak, silakan klik konsultasi online.
            <strong class="blue-important-text bold-text">Jika Anda masih memiliki pertanyaan dan ingin berkonsultasi, dokter 24-jam online satu-satu analisis untuk Anda </strong><img alt="" src="../assets/images/red-right.gif" style="width: 26px; height: 15px;">
          </p> -->

      </div>

      <!-- from here -->
      <div class="square-color">
        <a href="tel:081362621616" style="background:#8dd8cc">
            <font>Konsultasi</font>
        </a>
        <a href="javascript:void(0)" onclick="onKST();" rel="external nofollow" style="background:#f2c769; float: right;">
            <font>online</font>
        </a>
        <a href="tel:081362621616" style="background:#8dd8cc">
          <font>melalui telepon</font>
        </a>
        <a href="javascript:void(0)" onclick="onKST();" rel="external nofollow" style="background:#f2c769">
          <font></font>
        </a>
      </div>

      <br>

      <div class="txt_title">[Artikel terkait]</div>

      <div class="txt_li ov">
          <ul class="txt_li_ul">
            <li>Sebelumnya : <a href='bahayaBalanitis.php'>Bahaya Penyakit Balanitis</a> </li>
            <li>Berikutnya : <a href='balanitis.php'>Balanitis</a> </li>
          </ul>
      </div>

    <?php include('../layouts2/gejala-tanyakan.php'); ?>

  <!-- </div> -->

  <?php include('../layouts2/footer.php'); ?>

</div>

<!-- </div> -->
<script type="text/javascript" src="../assets/js/jquery-3.3.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../assets/materialize/js/materialize.min.js" charset="utf-8"></script>
<!-- <script type="text/javascript" src="../assets/js/kst_m.js" charset="utf-8"></script> -->
<script type="text/javascript" src="../assets/js/ks.js?cI=605571&fI=63440&ism=1" charset="utf-8"></script>
<script type="text/javascript" src="../assets/js/onKST.js" charset="utf-8"></script>
<script type="text/javascript" src="../assets/js/nav.js" charset="utf-8"></script>

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mI6%2bCxw2h%2bwAuIszpmLL41U14ixeMvKXTVj6JIwOMVZy9AN8k5gW7naJubrEPLcxCbl2uk8vy%2fVPbZ52niVmAvtSIClZ5Hdx8C9zQj5BzWYej9NOupokAE4sEDnCbJKbXArxQi6iPlt%2f%2bBA2GY8qblZDvM3ywNFv3TlQLXpo4loJobe5DHt3fAZSwEVGnCsuupTfFHo26wX4aMQdF6Vb57QcgjTxH%2bDVb8HoJAJRocE%2bMa9gpUlo1fxtEJaGCCApj9RVcpVYxXm2GbKr%2f0jOqpxwVnhGLHPfEsWgb4VpgiJ4VPJrT5u%2fvqWxy%2fnJKCeT707mv2hdqZ%2f3xt9%2fwJYOid%2byk0iFFnFOodh4mYN8Rij9Tyl%2fOwSJkRtRlHpDh7CRl7wbqM74FxhglTNVFv82f%2fVuwBkN05fv2n977nNWNgLEoCYXxBcYrVqSbXM0SCPsuQE7JgXW9YGYj4niIx%2bwtv3ueO9LEPvmySB%2fqdjT4%2bKN7sAq2Oe%2fSU0GOtQvERAbBbqXeH2lJXtAXPdhOVsdRHHpg3Bam2MgvdVcrrgfY2iy4kirwRz0sXw%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};
</script>

<!-- New -->
<style>#zh_ztgg{display:none;}</style>
<script src="../assets/js/jquery-2.1.4.min.js" type="text/javascript"></script> 
<script type="text/javascript">
jQuery(function(a) {
    a(function() {
        var b;
        a("#rcslider").hover(function() {
            clearInterval(b)
        },
        function() {
            b = setInterval(function() {
                var b = a("#rcslider"),
                c = b.find("li:last").height();
                b.animate({
                    marginTop: c + 3 + "px"
                },
                1e3,
                function() {
                    b.find("li:last").prependTo(b),
                    b.find("li:first").hide(),
                    b.css({
                        marginTop: 0
                    }),
                    b.find("li:first").fadeIn(1e3)
                })
            },
            3e3)
        }).trigger("mouseleave")
    }),
    a(document).ready(function() {
        a("#rcslider li").hover(function() {
            a(this).stop().fadeTo(300, 1)
        },
        function() {
            a(this).stop().fadeTo(300, .6)
        })
    })
});
</script>

<script type="text/javascript" src="../assets/js/jquery-3.3.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../assets/MDL/material.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../assets/js/kst_m.js" charset="utf-8"></script>
<!-- <script type="text/javascript" src="https://vip3-kf9.kuaishang.cn/bs/ks.js?cI=605571&fI=63440&ism=1" charset="utf-8"></script> -->
<script type="text/javascript" src="../assets/js/ks.js?cI=605571&fI=63440&ism=1" charset="utf-8"></script> 
<script type="text/javascript" src="../assets/js/onKST.js" charset="utf-8"></script>

<!-- for list dropdown -->
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    // $('select').material_select();
    $('li').attr("class", "browser-default");
  });
</script>


<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mEtG4rrtm6Vos4fOht%2fjY%2bTrbzz66VlCBLhU15lpVydl%2flCQaoLxyFZafOj1It4d9GdVGwanJ6RM1yeX%2bc6yJL8CiIKp5mapRfhp30OQvmjOH9i2XNpYZAHOjGbv1QrxJogagDUHSim%2bhnv8H6Q81Lv47fm1vGir7LWYhyaEGbM39rls6Z0ozUqbQp3fS1UHB2zTFbyI8xOGup7OK%2f7fwC%2fnmZNPfP1VFHwMLHyFd7xMnR1yRIr9rQvvlbt2ssoDYafJvGrAwTO280e%2b8VT0Owrk53wTbuJ%2bzQ9H%2fZNwyTbAmrREy3jH%2f9MLjaGKVBX7X%2bQUD3K2oVE9oUrQpdqKAAMruXBgaAN4sW8Up5hY9U1W53S3OdN6B8fHhY0%2fS58x%2bRO%2fDmp5c0IRXFKUCDUqabjqneIMJXqiW62rWFEth3JYiGobH716B%2fmqa6b5TUHuNOvxXnZrzJjUEwgQJLf59i62I20iBRfSxjIJX1P2aTiY4HKlUJNv%2bMpmakN%2fhfiZyrHfW%2fESvBYjjZlWzStoUkw6ye3B08Dqc" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>

</body>
</html>